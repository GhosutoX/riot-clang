/* Autogenerated by mlir-tblgen; don't manually edit */

#ifdef GEN_PASS_DECL
// Generate declarations for all passes.
#define GEN_PASS_DECL_ENABLEARMSTREAMING
#undef GEN_PASS_DECL
#endif // GEN_PASS_DECL

//===----------------------------------------------------------------------===//
// EnableArmStreaming
//===----------------------------------------------------------------------===//
#ifdef GEN_PASS_DECL_ENABLEARMSTREAMING
struct EnableArmStreamingOptions {
  mlir::arm_sme::ArmStreaming mode = mlir::arm_sme::ArmStreaming::Default;
};
#undef GEN_PASS_DECL_ENABLEARMSTREAMING
#endif // GEN_PASS_DECL_ENABLEARMSTREAMING
#ifdef GEN_PASS_DEF_ENABLEARMSTREAMING
namespace impl {

template <typename DerivedT>
class EnableArmStreamingBase : public ::mlir::OperationPass<mlir::func::FuncOp> {
public:
  using Base = EnableArmStreamingBase;

  EnableArmStreamingBase() : ::mlir::OperationPass<mlir::func::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  EnableArmStreamingBase(const EnableArmStreamingBase &other) : ::mlir::OperationPass<mlir::func::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("enable-arm-streaming");
  }
  ::llvm::StringRef getArgument() const override { return "enable-arm-streaming"; }

  ::llvm::StringRef getDescription() const override { return "Enable Armv9 Streaming SVE mode"; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("EnableArmStreaming");
  }
  ::llvm::StringRef getName() const override { return "EnableArmStreaming"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<func::FuncDialect>();

  }

  /// Explicitly declare the TypeID for this class. We declare an explicit private
  /// instantiation because Pass classes should only be visible by the current
  /// library.
  MLIR_DEFINE_EXPLICIT_INTERNAL_INLINE_TYPE_ID(EnableArmStreamingBase<DerivedT>)

  EnableArmStreamingBase(const EnableArmStreamingOptions &options) : EnableArmStreamingBase() {
    mode = options.mode;
  }
protected:
  ::mlir::Pass::Option<mlir::arm_sme::ArmStreaming> mode{*this, "mode", ::llvm::cl::desc("Select how streaming-mode is managed at the function-level."), ::llvm::cl::init(mlir::arm_sme::ArmStreaming::Default), ::llvm::cl::values(
                clEnumValN(mlir::arm_sme::ArmStreaming::Default, "default",
						   "Streaming mode is part of the function interface "
						   "(ABI), caller manages PSTATE.SM on entry/exit."),
                clEnumValN(mlir::arm_sme::ArmStreaming::Locally, "locally",
						   "Streaming mode is internal to the function, callee "
						   "manages PSTATE.SM on entry/exit.")
          )};
private:
};
} // namespace impl
#undef GEN_PASS_DEF_ENABLEARMSTREAMING
#endif // GEN_PASS_DEF_ENABLEARMSTREAMING
#ifdef GEN_PASS_REGISTRATION

//===----------------------------------------------------------------------===//
// EnableArmStreaming Registration
//===----------------------------------------------------------------------===//

inline void registerEnableArmStreaming() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return mlir::arm_sme::createEnableArmStreamingPass();
  });
}

// Old registration code, kept for temporary backwards compatibility.
inline void registerEnableArmStreamingPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return mlir::arm_sme::createEnableArmStreamingPass();
  });
}

//===----------------------------------------------------------------------===//
// ArmSME Registration
//===----------------------------------------------------------------------===//

inline void registerArmSMEPasses() {
  registerEnableArmStreaming();
}
#undef GEN_PASS_REGISTRATION
#endif // GEN_PASS_REGISTRATION
// Deprecated. Please use the new per-pass macros.
#ifdef GEN_PASS_CLASSES

template <typename DerivedT>
class EnableArmStreamingBase : public ::mlir::OperationPass<mlir::func::FuncOp> {
public:
  using Base = EnableArmStreamingBase;

  EnableArmStreamingBase() : ::mlir::OperationPass<mlir::func::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  EnableArmStreamingBase(const EnableArmStreamingBase &other) : ::mlir::OperationPass<mlir::func::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("enable-arm-streaming");
  }
  ::llvm::StringRef getArgument() const override { return "enable-arm-streaming"; }

  ::llvm::StringRef getDescription() const override { return "Enable Armv9 Streaming SVE mode"; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("EnableArmStreaming");
  }
  ::llvm::StringRef getName() const override { return "EnableArmStreaming"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<func::FuncDialect>();

  }

  /// Explicitly declare the TypeID for this class. We declare an explicit private
  /// instantiation because Pass classes should only be visible by the current
  /// library.
  MLIR_DEFINE_EXPLICIT_INTERNAL_INLINE_TYPE_ID(EnableArmStreamingBase<DerivedT>)

protected:
  ::mlir::Pass::Option<mlir::arm_sme::ArmStreaming> mode{*this, "mode", ::llvm::cl::desc("Select how streaming-mode is managed at the function-level."), ::llvm::cl::init(mlir::arm_sme::ArmStreaming::Default), ::llvm::cl::values(
                clEnumValN(mlir::arm_sme::ArmStreaming::Default, "default",
						   "Streaming mode is part of the function interface "
						   "(ABI), caller manages PSTATE.SM on entry/exit."),
                clEnumValN(mlir::arm_sme::ArmStreaming::Locally, "locally",
						   "Streaming mode is internal to the function, callee "
						   "manages PSTATE.SM on entry/exit.")
          )};
};
#undef GEN_PASS_CLASSES
#endif // GEN_PASS_CLASSES
